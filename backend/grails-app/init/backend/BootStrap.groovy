package backend
import java.time.LocalDate
import java.time.LocalTime
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.text.SimpleDateFormat
import java.text.DateFormat


import java.time.temporal.ChronoUnit;

class BootStrap {

    def init = { servletContext ->
        Random rand = new Random()
        final float UBER_BASE_PRICE = 80.0
        final float FORD_BASE_PRICE = 69.0
        final float AMAZON_BASE_PRICE = 115.0

        Company companyAmazon = new Company(name:"AMAZON", segment:"tech", base_price: AMAZON_BASE_PRICE).save()
        Company companyFord = new Company(name:"FORD", segment:"auto",  base_price: FORD_BASE_PRICE).save()
        Company companyUber = new Company(name:"UBER", segment:"transport", base_price: UBER_BASE_PRICE).save()

        Company[] companies = new Company[3]

        companies[0] = companyAmazon
        companies[1] = companyFord
        companies[2] = companyUber


        LocalDate actualDate = LocalDate.now();
        LocalDate endDate = actualDate.minus(30, ChronoUnit.DAYS)

        LocalTime openMarket = LocalTime.parse("10:00:00", DateTimeFormatter.ISO_TIME); 
        LocalTime closeMarket = LocalTime.parse("18:00:00", DateTimeFormatter.ISO_TIME);

        for (LocalDate date = endDate; date.isBefore(actualDate); date = date.plusDays(1))
        {
            for(LocalTime time = openMarket; time.isBefore(closeMarket); time = time.plusHours(1))
            {
                for(int minute = 0; minute < 60; minute++){
                    LocalTime finalTime = time.plusMinutes(minute)  

                    LocalDateTime finalStockDateTimeVariation = LocalDateTime.of(date, finalTime)

                    for(Company company: companies){
                        println finalStockDateTimeVariation

                        Stock stock = new Stock(company: company, price: company.base_price + ((company.base_price * 0.03) * rand.nextFloat()), date: java.sql.Timestamp.valueOf(finalStockDateTimeVariation)).save()
                    }

                }
            }
        }

        println "INITIALIZING"
    }
  
}
 