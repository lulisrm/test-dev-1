package backend

class Company {
    
    String name
    String segment
    float base_price
  
    static hasMany = [stocks: Stock]

    static constraints = {
       name(blank:false, unique:true)
       segment(blank: false, unique: false)
       base_price(blank: false)
    }
}
