package backend

class Stock {

    float price
    Date date
    Company company

    static belongsTo = [ company: Company ]

    static constraints = {
        price(blank:true)
        date(blank: true, unique: false)
        company(unique: false)
    }
}
